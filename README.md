# Tarea 1

En este repo puede encontrar los siguientes archivos:

* [Enunciado de la tarea](./pds_tarea1_c3_2022-1.pdf)
* [Solución del ejercicio 1](./TheoreticalPart.pdf)
* [Solución del ejercicio 2](./TheoreticalPart.pdf)
* [Solución del ejercicio 3](./exercise3.py)

Para el **ejercicio #3**, debe tener instaladas las siguientes dependencias:

* Python 3
* Numpy
* Matplotlib

Las versiones probadas en este caso:

* Python 3 === 3.8.5
* Numpy === 1.19.1
* Matplotlib === 3.2.2

Puede instalar dichas dependencias usando `PIP`:

```bash
pip3 install numpy matplotlib
```

Para usar el programa, por favor, seguir las siguientes instrucciones:

1. Abrir el programa:

```bash
FREQ=1000
SAMPLING=5000
CYCLES=5
python3 ./exercise3.py ${FREQ} ${SAMPLING} ${CYCLES}
```

donde `FREQ` es la frecuencia fundamental de la onda senoidal contínua, `SAMPLING` es la frecuencia de muestreo y `CYCLES` es el número de ciclos a graficar con respecto a la onda senoidal contínua.
