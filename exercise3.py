#!/bin/bash
import sys
import matplotlib.pyplot as plt
import numpy as np
from fractions import Fraction

figure = 1

def plot_original(f, c, samples=500):
    global figure
    period = 1 / f
    frange = period * c
    domain = np.linspace(0, frange, samples)
    cdomain = 2 * np.pi * f * domain
    function = np.sin
    plt.figure(figure)
    plt.plot(domain, function(cdomain), "C1", label="x(t)")

def plot_discrete(f, fs, c):
    global figure
    # Total range
    frange = c / f
    # Sample steps
    sample_period = 1 / fs
    domain = np.arange(0, frange + sample_period, sample_period)
    ddomain = 2 * np.pi * f * domain
    function = np.sin
    plt.figure(figure)
    plt.stem(domain, function(ddomain), markerfmt='bo', label="x(n)", use_line_collection=True)

def get_alias(f, fs):
    
    while(f >= fs):
        f =  f - fs
            
    if(f < fs / 2):
        return f
    else:
        return f - fs

def plot_alias (f, fs, c, samples=500):
    global figure
    falias = get_alias (f, fs)
    # Plot using the original parameters - domain
    period = 1 / f
    frange = period * c
    domain = np.linspace(0, frange, samples)
    # Scaling the domain to the right value
    cdomain = 2 * np.pi * falias * domain
    function = np.sin
    plt.figure(figure)
    plt.plot(domain, function(cdomain), "C2", label="x_alias(t)")
    print("\tFreq Alias: ", falias, "Hz. Fraction:", Fraction(falias / fs))
    print("\tFreq Discrete: ", f, "Hz. Fraction", Fraction(f / fs))


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("The program must have 3 arguments: " + \
        "(1) Natural frequency of the analog signal, " + \
        "(2) Sampling frequency, (3) Number of periods to plot")
        sys.exit(-1)
    
    nfrequency = float(sys.argv[1])
    samplingf = float(sys.argv[2])
    cycles = float(sys.argv[3])
    print("Solving for")
    print("\tFreq Continuous: ", nfrequency)
    print("\tSampling Freq: ", samplingf)
    print("\tCycles: ", cycles)
    plot_original (nfrequency, cycles)
    plot_discrete (nfrequency, samplingf, cycles)
    plot_alias (nfrequency, samplingf, cycles)
    plt.legend()
    plt.ylabel("x(t)")
    plt.xlabel("t(s)")
    plt.xticks(rotation=90)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.tight_layout()
    plt.show()
