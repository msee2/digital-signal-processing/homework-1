# Tarea 1

Luis Gerardo Leon Vega - Carnet: 2014069639



1. Determine si las siguientes señales son periódicas. En caso afirmativo, especifique su frecuencia fundamental.

* $x_a(t) = 3\cos(3t + \pi/6)$

Toda función cosenoidal con variable independiente dentro es periódica. En este caso, la frecuencia es $f = 3/2\pi$

* $x(n) = 3\cos(5n + \pi/6)$

En este caso, la función cosenoidal es discreta. Es periódica si la frecuencia fundamental es racional y menor que 1. Para este caso, $2 \pi F = 5$, por tanto, la frecuencia es $5/2\pi$, lo cual no es racional dado que $\pi$ es irracional.

*  $x(n) = 2 e^{j(n/6 - \pi)}$

La exponencial compleja tiene un comportamiento oscilatorio como una senoidal. Como en este caso, la señal es discreta, la frecuencia fundamental, al ser despejada, es: $2\pi F = n/6 \rightarrow F = n / 12 \pi$, lo cual no es racional dado que  $\pi$ es irracional..

* $x(n) = \cos(n/8)cos(\pi n/8)$

En este caso, la señal es discreta. Por identidades trigonométricas
$$
\cos(x)\cos(y) = \frac{1}{2}\cos(x+y)+\frac{1}{2}\cos(x-y)
$$

$$
= \frac{1}{2}\cos\left(\frac{n(1+\pi)}{8}\right)+\frac{1}{2}\cos\left(\frac{n(1-\pi)}{8}\right)
$$

La suma de señales cosenoidales es periódica siempre y cuando sus componentes sean periódicas. Tomando la primer componente, $2\pi F = \frac{1 + \pi}{8} \rightarrow F = \frac{1+\pi}{16 \pi}$. Dado que la frecuencia fundamental es irracional, la señal no es periódica.

* $x(n) = \cos(n\pi/2) - \sin(n\pi/8) + 3\cos(n\pi/4 + \pi/3)$

Como en el caso anterior, los componentes deben ser periódicos para que la señal completa sea periódica.

Para la primer componente:
$$
\cos(n \pi / 2) \rightarrow 2\pi F = \pi /2 \rightarrow F = 1/4
$$
por lo que tiene una frecuencia de 4 muestras por periodo.

Para la segunda componente:
$$
\sin(n \pi / 8) \rightarrow 2\pi F = \pi /8 \rightarrow F = 1/16
$$
por lo que tiene una frecuencia de 16 muestras por periodo.

Para la última componente:
$$
\cos(n \pi / 4 + \pi /3) \rightarrow 2\pi F = \pi /4 \rightarrow F = 1/8
$$
por lo que tiene una frecuencia de 8 muestras por periodo.

Dado que todas las frecuencias son racionales, todas las componentes son periódicas, dando que la función es **periódica**. La frecuencia fundamental está dada por el mínimo común múltiplo, que da $mcm = 16$ muestras por periódo.



2. Una comunicación digital utiliza símbolos en código binario que representan muestras de una
   señal de entrada:

$$
x_a(t) = 3\cos(600\pi t) + 2 \cos(1800 \pi t)
$$

El enlace opera a 10.000 bits/s y cada muestra está cuantizada en 1024 diferentes niveles de
voltaje

* Cuál es la frecuencia de muestreo y la frecuencia de plegado?

Cada símbolo requiere 10 bits (1024 niveles), por lo que la frecuencia de muestreo debe ser $10000$ bits/s / $10$ bits = **$F_s = 1000$ Hz (1kHz)**. La frecuencia de plegado se determina como $F_s/2 = 500 Hz$.

* Cuál es la tasa de Nyquist para la señal $x_a(t)$?

La tasa de Nyquist ($f_n$) está dada por el doble del ancho de banda requerido (o la frecuencia fundamental) de la señal con más frecuencia. En este caso, la señal es la suma de dos componentes cosenoidales con frecuencias $f_1 = 300 Hz$ y $f_2 = 900 Hz$. Dado que $f_2$ es la más rápida, $f_n = 2 f_2 = 2 \cdot 900 Hz = 1800 Hz$.

* Cuáles son las frecuencias resultantes en tiempo discreto de la señal $x(n)$?

La frecuencia fundamental está dada por: $F = f/F_s$ 

$F_1 = 300 Hz / 1000 Hz = 3/10$

Sin embargo, tenemos que para $F_2$

$F_2 = 900 Hz / 1000 Hz = 9/10$

lo cual, es mayor a $1/2$. Para ello, se ve como un alias de $|f_2 - F_s| = 100 Hz$

* Cuál es la resolución Δ?

Para este caso, asumiendo que la señal $x_a(t)$ es de tensión eléctrica, se requieren al menos  $Vpp = 2Vp = 2 \times (A_1 + A_2) = (2 \times 5V) = 10V$ para satisfacer la señal en caso de tener la máxima amplitud en ambas componentes.

Esto implica que la resolución sea $\Delta=10$ V/$1024$ = $0.00977$ V

* Cuál es el SQNR de la señal discreta con menor frecuencia?

La señal de menor frecuencia está dada por $F_1 = 3/10$. En este caso, el SQNR se puede calcular como $\text{SQNR} = P_x/P_n$, donde $P_x$ es la potencia de la señal $x_1$ (de menor frecuencia) y $P_n$ es la potencia del ruido de cuantificación. El ruido de cuantificación está distribuido uniformemente entre $-\Delta/2$ y $\Delta/2$. Por tanto, después de integrar el ruido cuadrático en el rango de validez:
$$
P_n = \frac{\Delta ^2}{12}
$$
En cuanto a la señal, $P_x = E[x^2(n)]$. Para este caso, $x_1(n) = 3 \cos(2\pi F_1n)$. Para este caso, la función es periódica y cosenoidal. Su potencia media es usualmente dada por $A^2 /2$, donde $A = 3$ para el caso de $x_1(n)$. Por lo tanto:
$$
\text{SQNR} = \frac{P_x}{P_q} = \frac{A^2/2}{\Delta^2/12} = \frac{6A^2}{\Delta^2}
$$
Tomando $A=3$ y $\Delta = 0.00977$:
$$
\text{SQNR} = \frac{6 \times 3^2}{0.00977^2} = 565724.04
$$
Y en forma de logaritmo:
$$
\text{SQNR}_{10} = 10\log_{10} \left(567724.04\right) = 57.54 \text{dB}
$$




